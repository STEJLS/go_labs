//Package XMLParsing  provides to work with xml files
package XMLParsing

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
)

//Config is structure for parsing XML file
type Config struct {
	HTTP Http     `xml:"http"`
	Db   DataBase `xml:"DataBase"`
}

//Http is structure for parsing XML file
type Http struct {
	XMLName xml.Name `xml:"http"`
	Port    int      `xml:"port,attr"`
	Timeout int      `xml:"timeout,attr"`
}

//DataBase is structure for parsing XML file
type DataBase struct {
	XMLName  xml.Name `xml:"DataBase"`
	Host     string   `xml:"host"`
	User     string   `xml:"user"`
	DBName   string   `xml:"dbname"`
	Password string   `xml:"password"`
}

//Formatted output Config
func (config Config) String() string {
	return fmt.Sprintf("Config:\n\t%v\n\t%v", config.HTTP, config.Db)
}

//Formatted output Http
func (_http Http) String() string {
	return fmt.Sprintf("http:\n"+
		"\tport : %v\n"+
		"\ttimeout : %v\n",
		_http.Port, _http.Timeout)
}

//Formatted output DataBase
func (db DataBase) String() string {
	return fmt.Sprintf("DataBase:\n"+
		"\thost : %v\n"+
		"\tuser : %v\n"+
		"\tdbname : %v\n"+
		"\tpassword : %v",
		db.Host, db.User, db.DBName, db.Password)
}

//XMLParsing is The function parsing XML file located at "source" by "query".
//Query have to describes  XML file's structure
//log - logger object.
func XMLParsing(source string, query interface{}, log *log.Logger) error {
	content, err := ioutil.ReadFile(source)
	if err != nil {
		log.Printf("Fatal. Couldn't open XML config (%v) for parsing.", source)
		return err
	}

	err = xml.Unmarshal(content, query)
	if err != nil {
		log.Printf("Fatal. Couldn't Unmarshal by file(%v).", source)
		return err
	}

	log.Printf("Info. Parsing %v successfully completed.", source)
	return nil
}
