package MongoDB

import (
	"log"

	"gopkg.in/mgo.v2"
)

//Point2D describe point in 2D coordinates
type Point2D struct {
	X int `json:"x" bson: "x"`
	Y int `json:"y" bson:"y"`
}

var session *mgo.Session
var collection *mgo.Collection

//ConnectToMongo open connection with MongoDB
func ConnectToMongo(host string, log *log.Logger) *mgo.Session {
	var err error
	session, err = mgo.Dial(host)

	if err != nil {
		log.Fatalln(err)
	}

	log.Printf("Info. Connection with MongoDB successfully completed.")
	return session
}

//ConnectToDBandColection connect to DB  and select collection.
func ConnectToDBandColection(DBname, collectionName string, log *log.Logger) {
	log.Printf("Info. Connection with DB and collection successfully completed.")
	collection = session.DB(DBname).C(collectionName)
}

//Insert inserting document into collection
func Insert(document interface{}, log *log.Logger) {
	err := collection.Insert(document)

	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Info. Insert to DB successfully completed.")
}

//Find provides the query to the collection
func Find(query, result interface{}, log *log.Logger) interface{} {
	err := collection.Find(query).All(result)

	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Info. Query successfully completed.")
	return result
}
