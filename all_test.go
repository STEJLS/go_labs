package main

import (
	"encoding/xml"
	"os"
	"testing"

	xmlP "bitbucket.org/STEJLS/go_labs/XMLParsing"
	"github.com/stretchr/testify/assert"
)

func GetConfig() xmlP.Config {
	config := xmlP.Config{
		xmlP.Http{
			xml.Name{"", ""},
			8080,
			5000,
		},
		xmlP.DataBase{
			xml.Name{"", ""},
			"aaaaaaaa",
			"bbbbbbbb",
			"fffffffff",
			"cccccccccc",
		},
	}
	return config
}

func TestMain(m *testing.M) {
	f := InitLogger("log.txt")
	result := m.Run()
	f.Close()
	os.Exit(result)
}

func TestXMLParsing(t *testing.T) {
	//Arrange
	expected := xmlP.Http{xml.Name{"", "http"}, 8080, 5000}
	var query xmlP.Http
	//ct
	xmlP.XMLParsing("test_config.xml", &query, Log)
	//Assert
	assert.Equal(t, expected, query)
}
func TestValidating(t *testing.T) {
	//Arrange
	config := GetConfig()
	//Act
	err := Validating(config)
	//Assert
	assert.NoError(t, err)
}

func TestValidatingByHTTP_Port(t *testing.T) {
	//Arrange #1
	config := GetConfig()
	config.HTTP.Port = 8081
	//Act
	err := Validating(config)
	//Assert
	assert.Error(t, err)

	//Arrange #2
	config.HTTP.Port = 10000
	//Act
	err = Validating(config)
	//Assert
	assert.Error(t, err)

	//Arrange #3
	config.HTTP.Port = 1024
	//Act
	err = Validating(config)
	//Assert
	assert.Error(t, err)

	//Arrange #4
	config.HTTP.Port = 100
	//Act
	err = Validating(config)
	//Assert
	assert.Error(t, err)

	//Arrange #5
	config.HTTP.Port = 1025
	//Act
	err = Validating(config)
	//Assert
	assert.NoError(t, err)

	//Arrange #6
	config.HTTP.Port = 8080
	//Act
	err = Validating(config)
	//Assert
	assert.NoError(t, err)

	//Arrange #7
	config.HTTP.Port = 3000
	//Act
	err = Validating(config)
	//Assert

}

func TestValidatingByHTTP_Timeout(t *testing.T) {
	//Arrange #1
	config := GetConfig()
	config.HTTP.Timeout = -1
	//Act
	err := Validating(config)
	//Assert
	assert.Error(t, err)

	//Arrange #2
	config.HTTP.Timeout = -100
	//Act
	err = Validating(config)
	//Assert
	assert.Error(t, err)

	//Arrange #3
	config.HTTP.Timeout = 10001
	//Act
	err = Validating(config)
	//Assert
	assert.Error(t, err)

	//Arrange #4
	config.HTTP.Timeout = 100000
	//Act
	err = Validating(config)
	//Assert
	assert.Error(t, err)

	//Arrange #5
	config.HTTP.Timeout = 0
	//Act
	err = Validating(config)
	//Assert
	assert.NoError(t, err)

	//Arrange #6
	config.HTTP.Timeout = 10000
	//Act
	err = Validating(config)
	//Assert
	assert.NoError(t, err)

	//Arrange #7
	config.HTTP.Timeout = 5000
	//Act
	err = Validating(config)
	//Assert
	assert.NoError(t, err)
}

func TestValidatingByDB_DBName(t *testing.T) {
	//Arrange #1
	config := GetConfig()
	config.Db.DBName = "four"
	//Act
	err := Validating(config)
	//Assert
	assert.Error(t, err)

	//Arrange #2
	config.Db.DBName = "a"
	//Act
	err = Validating(config)
	//Assert
	assert.Error(t, err)

	//Arrange #3
	config.Db.DBName = "fivef"
	//Act
	err = Validating(config)
	//Assert
	assert.NoError(t, err)

	//Arrange #4
	config.Db.DBName = "moreThanFive"
	//Act
	err = Validating(config)
	//Assert
	assert.NoError(t, err)
}

func TestValidatingByDB_Password(t *testing.T) {
	//Arrange #1
	config := GetConfig()
	config.Db.Password = "sixsix"
	//Act
	err := Validating(config)
	//Assert
	assert.Error(t, err)

	//Arrange #2
	config.Db.Password = "few"
	//Act
	err = Validating(config)
	//Assert
	assert.Error(t, err)

	//Arrange #3
	config.Db.Password = "sevenSe"
	//Act
	err = Validating(config)
	//Assert
	assert.NoError(t, err)

	//Arrange #4
	config.Db.Password = "moreThаnSeven"
	//Act
	err = Validating(config)
	//Assert
	assert.NoError(t, err)
}

func TestValidatingByDB_User(t *testing.T) {
	//Arrange #1
	config := GetConfig()
	config.Db.User = "four"
	//Act
	err := Validating(config)
	//Assert
	assert.Error(t, err)

	//Arrange #2
	config.Db.User = "few"
	//Act
	err = Validating(config)
	//Assert
	assert.Error(t, err)

	//Arrange #3
	config.Db.User = "fiveF"
	//Act
	err = Validating(config)
	//Assert
	assert.NoError(t, err)

	//Arrange #4
	config.Db.User = "moreThаnFive"
	//Act
	err = Validating(config)
	//Assert
	assert.NoError(t, err)
}

func TestValidatingByDB_Host(t *testing.T) {
	//Arrange #1
	config := GetConfig()
	config.Db.Host = "four"
	//Act
	err := Validating(config)
	//Assert
	assert.Error(t, err)

	//Arrange #2
	config.Db.Host = "few"
	//Act
	err = Validating(config)
	//Assert
	assert.Error(t, err)

	//Arrange #3
	config.Db.Host = "fiveF"
	//Act
	err = Validating(config)
	//Assert
	assert.NoError(t, err)

	//Arrange #4
	config.Db.Host = "moreThаnFive"
	//Act
	err = Validating(config)
	//Assert
	assert.NoError(t, err)
}
