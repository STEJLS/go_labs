//main package is designed for laboratory works in the astral. It open xml config, parsing , validating it and covers unit test
package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/user"

	mongo "bitbucket.org/STEJLS/go_labs/MongoDB"
	xmlP "bitbucket.org/STEJLS/go_labs/XMLParsing"
)

//Log is global variable for logging throughout the program
var Log *log.Logger

//LogSource is global variable for command flag
// uses for getting output file for logging
var LogSource string

//ConfigSource is global variable for command flag
// uses for getting input config file
var ConfigSource string

//InitLogger is the function that init global variable Log
//by "destination" file. It returns *os.File so you have to close
//the file before exiting the program.
func InitLogger(destination string) *os.File {
	file, err := os.OpenFile(destination, os.O_APPEND|os.O_CREATE, 0666)
	if err != nil {
		log.Fatalln("Warn. Failed to open log file %q :", destination, err)
	}

	user, _ := user.Current()
	Log = log.New(file, user.Name+" ", log.LstdFlags|log.Lshortfile)

	Log.Printf("Info. The log file (%v) has been successfully installed.", destination)
	return file
}

//InitFlags is the function that init Command-Line Flags
func InitFlags() {
	flag.StringVar(&LogSource, "log_source", "log.txt", "Source for log file")
	flag.StringVar(&ConfigSource, "config_source", "config.xml", "Source for config file")
	flag.Parse()
}

// Validating is the function that check input information
// on the internal application rules
func Validating(info xmlP.Config) error {
	if info.HTTP.Port <= 1024 || info.HTTP.Port > 8080 {
		str := fmt.Sprintf("Warn. Don't valid info in port %v", info.HTTP.Port)
		Log.Printf(str)
		return fmt.Errorf(str)
	}

	if info.HTTP.Timeout < 0 || info.HTTP.Timeout > 10000 {
		str := fmt.Sprintf("Warn. Don't valid info in timeout %v", info.HTTP.Timeout)
		Log.Printf(str)
		return fmt.Errorf(str)
	}

	if len(info.Db.DBName) < 5 {
		str := fmt.Sprintf("Warn. Don't valid info in dbname(length = %v)", len(info.Db.DBName))
		Log.Printf(str)
		return fmt.Errorf(str)
	}

	if len(info.Db.Password) < 7 {
		str := fmt.Sprintf("Warn. Don't valid info in password(lenght = %v)", len(info.Db.Password))
		Log.Printf(str)
		return fmt.Errorf(str)
	}

	if len(info.Db.User) < 5 {
		str := fmt.Sprintf("Warn. Don't valid info in user(lenght = %v)", len(info.Db.User))
		Log.Printf(str)
		return fmt.Errorf(str)
	}

	if len(info.Db.Host) < 5 {
		str := fmt.Sprintf("Warn. Don't valid info in host(lenght = %v)", len(info.Db.Host))
		Log.Printf(str)
		return fmt.Errorf(str)
	}

	Log.Printf("Info. The information has been successfully validated.")
	return nil
}

//GetConfig open config file, parsing,
//validating them , and return configurations
func GetConfig() xmlP.Config {
	var config xmlP.Config
	err := xmlP.XMLParsing(ConfigSource, &config, Log)
	if err != nil {
		log.Fatalln(err)
	}

	err = Validating(config)
	if err != nil {
		log.Fatalln(err)
	}
	return config
}

//GetInfoFromDB connect to db, insert document and
//extract them from db
func GetInfoFromDB(config xmlP.Config) []mongo.Point2D {
	session := mongo.ConnectToMongo(config.Db.Host, Log)
	defer session.Close()

	mongo.ConnectToDBandColection(config.Db.DBName, "points", Log)

	p := mongo.Point2D{111, 112}
	mongo.Insert(p, Log)

	result := []mongo.Point2D{}
	mongo.Find(nil, &result, Log)

	return result
}

func main() {
	InitFlags()

	logFile := InitLogger(LogSource)
	defer logFile.Close()

	config := GetConfig()
	fmt.Println(config)

	result := GetInfoFromDB(config)
	for _, item := range result {
		fmt.Printf("x : %v Y : %v\n", item.X, item.Y)
	}

	Log.Printf("Info. The program is successfully completed.")
}
